#!/usr/bin/env php
<?php

$auditjs_out_str = <<<EOS1

{auditjs cli text output goes here}

EOS1;

/* helper functions */
function pkg_url( string $package ) {
	if ( strstr( $package, "@" ) ) {
		$package = substr( $package, 0, strpos( $package, "@" ) );
	}
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

function advisory_link( string $url ) {
	return "[[ {$url} | advisory link ]]";
}

function trim_long( string $str, $len = 32 ) {
	if ( strlen( $str ) > $len ) {
		$str = substr( $str, 0, $len ) . "...";
	}

	// find some vuln texts
	if ( preg_match( "/(CWE\-(\d+))\:/", $str, $m ) ) {
		$str = str_replace( $m[1], "[[ https://cwe.mitre.org/data/definitions/{$m[2]}.html | {$m[1]} ]]", $str );
	}
	if ( preg_match( "/(\[(CVE\-\d+\-\d+)\])/", $str, $m ) ) {
		$str = str_replace( $m[1], "[[ https://nvd.nist.gov/vuln/detail/{$m[2]} | {$m[2]} ]]:", $str );
	}

	return $str;
}

function service() {
	return "[[ https://github.com/sonatype-nexus-community/auditjs | auditjs ]]";
}

function get_risk( $val ) {
	$risks = [
		'info' => "{icon info-circle color=blue} **informational**",
		'low' => "{icon check-circle color=green} **low**",
		'medium' => "{icon exclamation-triangle color=yellow} **medium**",
		'high' => "{icon exclamation-triangle color=orange} **high**",
		'critical' => "{icon exclamation-triangle color=red} **critical**"
	];

	switch ( true ) {
	case $val < 4 && (float)$val > 0.1:
		$risk = 'low';
		break;
	case $val < 7 && $val >= 4:
		$risk = 'medium';
		break;
	case $val < 9 && $val >= 7:
		$risk = 'high';
		break;
	case $val < 11 && $val >= 9:
		$risk = 'critical';
		break;
	default:
		$risk = 'info';
		break;
	}
	return $risks[$risk];
}

// phab output
echo <<<EOS2
| Vulnerability | Package | Notes | Service | Remediation | Risk
| ---- | ---- | ---- | ---- | ---- | ---- 

EOS2;

// helper vars
$package = 'NO PACKAGE';
$version = 'NO VERSION';
$c1 = 0;
$vulns = [];

$processed_array = explode( "\n", $auditjs_out_str );

foreach ( $processed_array as $line ) {
	// find package, version
	if ( preg_match( "/\-\ pkg\:(npm|pnpm|yarn)\/(.+)\@(.+)\ \-/", $line, $m ) ) {
		$package = trim( $m[2] ) ?? "NO PACKAGE";
		$version = trim( $m[3] ) ?? "NO VERSION";
		$c1 = 0;
	}

	 // vuln title
	if ( preg_match( "/Vulnerability\ Title\:\ (.+)/", $line, $m ) ) {
		$vulns["$package@$version"][$c1]['title'] = trim( $m[1] ) ?? "NO TITLE";
	}

	// vuln desc
	if ( preg_match( "/Description\:\ (.+)/", $line, $m ) ) {
		$vulns["$package@$version"][$c1]['description'] = trim( $m[1] ) ?? "NO DESC";
	}

	// cvss score for risk
	if ( preg_match( "/CVSS\ Score\:\ (.+)/", $line, $m ) ) {
		$vulns["$package@$version"][$c1]['risk'] = trim( $m[1] ) ?? "NO RISK";
	}

	// reference/adv url
	if ( preg_match( "/Reference\:\ (.+)/", $line, $m ) ) {
		$vulns["$package@$version"][$c1]['reference'] = trim( $m[1] ) ?? "NO REFERENCE";
		$c1++; // should be last field for a vulns
	}
}

// display in phab remarkup
foreach ( $vulns as $pkg => $vulndata ) {
	foreach ( $vulndata as $vulninfo ) {
		echo "| " . trim_long( $vulninfo['title'] ) . " | ";
		echo pkg_url( $pkg ) . " | ";
		echo advisory_link( $vulninfo['reference'] ) . " | ";
		echo service() . " | ";
		echo " [see details within advisory links] | ";
		echo get_risk( $vulninfo['risk'] );
		echo "\n";
	}
}
echo "\n";
