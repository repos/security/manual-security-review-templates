#!/usr/bin/env php
<?php

$npm_audit_str = <<<EOS1

{npm audit cli text output goes here}

EOS1;

// helper functions
function npm_pkg_url( $package ) {
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

function npm_adv_url( $url ) {
	return "[[ {$url} | advisory link ]]";
}

function service() {
	return "[[ https://github.com/advisories?query=type%3Areviewed+ecosystem%3Anpm | npm audit ]]";
}

function trim_long( string $str, $len = 32 ) {
	if ( strlen( $str ) > $len ) {
		$str = substr( $str, 0, $len ) . "...";
	}

	// find some vuln texts
	if ( preg_match( "/(CWE\-(\d+))\:/", $str, $m ) ) {
		$str = str_replace( $m[1], "[[ https://cwe.mitre.org/data/definitions/{$m[2]}.html | {$m[1]} ]]", $str );
	}
	if ( preg_match( "/(\[(CVE\-\d+\-\d+)\])/", $str, $m ) ) {
		$str = str_replace( $m[1], "[[ https://nvd.nist.gov/vuln/detail/{$m[2]} | {$m[2]} ]]:", $str );
	}

	return $str;
}

function get_risk( $risk ) {
	$risk = strtolower( $risk );
	$risks = [
		'low' => "{icon check-circle color=green} **low**",
		'moderate' => "{icon exclamation-triangle color=yellow} **medium**",
		'high' => "{icon exclamation-triangle color=orange} **high**",
		'critical' => "{icon exclamation-triangle color=red} **critical**"
	];

	if ( array_key_exists( $risk, $risks ) ) {
		return $risks[$risk];

	} else { return 'N/A';
	}
}

// phab output
echo <<<EOS2
| Vulnerability | Package | Notes | Service | Remediation | Risk
| ---- | ---- | ---- | ---- | ---- | ---- 
EOS2;

// helper vars
$return_string = "More info";
$count = 1;
$vuln = [];
$str_arr = explode( "\n", $npm_audit_str );

foreach ( $str_arr as $line ) {
	$line_arr = [];
	$line = str_replace( "│", "", $line );
	$line = trim( $line );
	$line_arr = preg_split( "/(\s\s)+/", $line );

	if ( preg_match( "/Package/", $line_arr[0] ) ) {
		$vuln['package'] = $line_arr[1];
	}
	if ( preg_match( "/Patched\ in/", $line_arr[0] ) ) {
		$vuln['version'] = str_replace( "||", "or", $line_arr[1] );
	}
	if ( preg_match( "/Path/", $line_arr[0] ) ) {
		$vuln['path'] = $line_arr[1];
	}
	if ( preg_match( "/low|medium|moderate|high|critical/", $line_arr[0] ) ) {
		$vuln['risk'] = $line_arr[0];
		$vuln['vuln'] = $line_arr[1];
	}
	if ( preg_match( "/More\ info/", $line_arr[0] ) ) {
		$vuln['info'] = $line_arr[1];
		$vuln['package'] = npm_pkg_url( $vuln['package'] ??= '' ) ?? "No package found";
		$vuln['version'] = $vuln['version'] ?? "N/A";
		$vuln['vuln'] = trim_long( $vuln['vuln'] ??= '', 42 ) ?? "N/A";
		$vuln['path'] = trim_long( $vuln['path'] ??= '', 42 ) ?? "N/A";
		$vuln['info'] = npm_adv_url( $vuln['info'] ) ?? "N/A";
		$vuln['service'] = service();
		$vuln['risk'] = get_risk( $vuln['risk'] ??= '' ) ?? "N/A";

		echo "| " . $vuln['vuln'] . " | " . $vuln['package'] .
			" | " . $vuln['path'] . " | " . $vuln['service'] .
			" | " . $vuln['info'] . " | " . $vuln['risk'];
		echo "\n";
	}
}
