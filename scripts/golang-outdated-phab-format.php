#!/usr/bin/env php
<?php

$golang_out_str = <<<EOS1

/* expected: ALL output generated via: go list -u -m -json all | go-mod-outdated */

EOS1;

function golang_pkg_url( $package ) {
	return "[[ https://{$package} | {$package} ]]";
}

// phab output
echo <<<EOS2
| Package | Current | Wanted
| ---- | ---- | ----

EOS2;

$count = 0;
$str_arr = explode( "\n", $golang_out_str );
foreach ( $str_arr as $line ) {
	$line_arr = [];
	$line_arr = preg_split( "/\|(\s)/", trim( $line ) );

	if ( count( $line_arr ) > 1 && $count > 1 ) {
		$line_arr = array_map( 'trim', $line_arr );
		$pkg = golang_pkg_url( $line_arr[1] ) ?? "No Package!";
		$ver_current = $line_arr[2] ?? "N/A";
		$ver_wanted = empty( $line_arr[3] ) ? "N/A" : $line_arr[3];

		echo "| $pkg";
		echo " | $ver_current";
		echo " | $ver_wanted";
		echo "\n";
	}
	$count++;
}
