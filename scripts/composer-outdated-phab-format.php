#!/usr/bin/env php
<?php

$composer_out_str = <<<EOS1

{composer outdated cli text output goes here}

EOS1;

function php_pkg_url( $package ) {
	return "[[ https://packagist.org/packages/{$package} | {$package} ]]";
}

// phab output
echo <<<EOS2
| Package| Current | Latest
| ---- | ---- | ---- 

EOS2;

$str_arr = explode( "\n", $composer_out_str );
foreach ( $str_arr as $line ) {
	$line = preg_replace( "/\│/", "", $line );
	$line_arr = preg_split( "/(\s)+/", trim( $line ) );
	if ( count( $line_arr ) > 1 ) {
		echo "| " . php_pkg_url( trim( $line_arr[0] ) );
		echo " | " . trim( $line_arr[1] );
		echo " | " . trim( $line_arr[2] );
		$notes = trim( implode( ' ', array_slice( $line_arr, 3 ) ) );
		if ( strlen( $notes ) > 80 ) {
			echo " | " . substr( $notes, 0, 80 ) . "...";

		} else { echo " | " . $notes;
		}
		echo "\n";
	}
}
