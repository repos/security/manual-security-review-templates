#!/usr/bin/env php
<?php

$npm_out_str = <<<EOS1

{npm outdated cli text ouput goes here}

EOS1;

function npm_pkg_url( $package ) {
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

// phab output
echo <<<EOS2
| Package| Current | Wanted | Latest | Depended By
| ---- | ---- | ---- | ---- | ----

EOS2;

$str_arr = explode( "\n", $npm_out_str );
foreach ( $str_arr as $line ) {
	$line_arr = [];
	$line = preg_replace( "/\│/", "", $line );
	$line_arr = preg_split( "/(\s\s)+/", trim( $line ) );
	if ( count( $line_arr ) > 1 ) {
		echo "| " . npm_pkg_url( trim( $line_arr[0] ) );
		echo " | " . trim( $line_arr[1] );
		echo " | " . trim( $line_arr[2] );
		echo " | " . trim( $line_arr[3] );
		echo " | " . trim( $line_arr[5] );
		echo "\n";
	}
}
