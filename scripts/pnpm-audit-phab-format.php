#!/usr/bin/env php
<?php

$npm_out_str = <<<EOS1

{pnpm audit cli text output goes here}

EOS1;

function npm_pkg_url( $package ) {
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

function npm_adv_url( $url ) {
	return "[[ {$url} | advisory link ]]";
}

// phab output
echo <<<EOS2
| Package| Vulnerable version | Patched version | Risk | Info
| ---- | ---- | ---- | ---- | ---- 

EOS2;

$count = 1;
$vuln = [];
$str_arr = explode( "\n", $npm_out_str );
foreach ( $str_arr as $line ) {
	$line_arr = [];
	$line = str_replace( "│", "", $line );
	$line = trim( $line );
	$line_arr = preg_split( "/(\s\s)+/", $line );

	if ( preg_match( "/Package/", $line_arr[0] ) ) {
		$vuln['package'] = $line_arr[1];
	}
	if ( preg_match( "/Vulnerable\ versions/", $line_arr[0] ) ) {
		$vuln['vuln_version'] = $line_arr[1];
	}
	if ( preg_match( "/Patched\ versions/", $line_arr[0] ) ) {
		$vuln['patch_version'] = $line_arr[1];
	}
	if ( preg_match( "/low|moderate|high|critical/", $line_arr[0] ) ) {
		$vuln['risk'] = $line_arr[0];
	}
	if ( preg_match( "/More\ info/", $line_arr[0] ) ) {
		$vuln['info'] = $line_arr[1];
		$vuln['package'] = npm_pkg_url( $vuln['package'] ??= "N/A" ) ?? "No package found";
		$vuln['vuln_version'] = $vuln['vuln_version'] ?? "N/A";
		$vuln['patch_version'] = $vuln['patch_version'] ?? "N/A";
		$vuln['risk'] = $vuln['risk'] ?? "N/A";
		$vuln['info'] = npm_adv_url( $vuln['info'] ) ?? "N/A";

		echo "| " . $vuln['package'] . " | " . $vuln['vuln_version'] .
			" | " . $vuln['patch_version'] . " | " . $vuln['risk'] .
			" | " . $vuln['info'];
		echo "\n";
	}
}
