#!/usr/bin/env php
<?php

// helper script to translate json output of various
// dependency vuln-checking tools (npm audit and snyk test for now)
// to Phab remarkup

declare( strict_types=1 );

// helpers
function cve_url( $id ) {
	return "[[ https://nvd.nist.gov/vuln/detail/$id | $id ]]";
}

function npm_pkg_url( $package ) {
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

function risk_icon( $risk ) {
	$risk_icons = [
		'info' => '{icon info-circle color=blue}',
		'low' => '{icon check-circle color=green}',
		'medium' => '{icon check-circle color=yellow}',
		'high' => '{icon check-circle color=orange}',
		'critical' => '{icon check-circle color=red}'
	];
	return $risk_icons[$risk];
}

function re_pipes( $str ) {
	return preg_replace( "/\|\|/", ";", $str );
}

function proc_npm_findings( $findings ) {
	$dep_paths = [];
	$dep_paths_str = 'found in: ';
	if ( is_array( $findings ) ) {
		foreach ( $findings as $v ) {
			if ( isset( $v['paths'] ) && is_array( $v['paths'] ) ) {
				foreach ( $v['paths'] as $v0 ) {
					$path_abrev = preg_split( "/\>/", $v0 )[0];
					if ( array_key_exists( $path_abrev, $dep_paths ) ) {
						$dep_paths[$path_abrev]++;
					} else {
						$dep_paths[$path_abrev] = 1;
					}
				}
			}
		}
	}
	foreach ( $dep_paths as $k => $v ) {
		$paths = ( $v > 1 ) ? "({$v} paths)" : "({$v} path)";
		$dep_paths_str .= npm_pkg_url( $k ) . " {$paths}";
		if ( $k !== array_key_last( $dep_paths ) ) {
			$dep_paths_str .= "; ";

		} else { $dep_paths_str .= " ";
		}
	}
	return $dep_paths_str;
}

// cli args
$mode = 'npm';
if ( isset( $argv[1] ) && $argv[1] == 'snyk' ) {
	$mode = 'snyk';
}

// read stdin, json expected
$json_data_string = '';
// phpcs:ignore
while ( ( $line = fgets( STDIN ) ) !== false ) {
	$json_data_string .= "$line";
}

$json_data_arr = json_decode( $json_data_string, true );
if ( $json_data_arr ) {
	// debug
	//var_dump( $json_data_arr );

	// phab output
	echo "| {icon info-circle color=blue} Vulnerability | Package | Notes | Service | Risk\n";
	echo "| ---- | ---- | ---- | ---- | ----\n";

	// npm
	if ( $mode == 'snyk' ) {
		$vulns_deduped = [];
		foreach ( $json_data_arr['vulnerabilities'] as $vuln ) {
			if ( array_key_exists( $vuln['id'], $vulns_deduped ) ) {
				$vulns_deduped[$vuln['id']]['path_count']++;
			} else {
				$vulns_deduped[$vuln['id']]['path_count'] = 1;
			}
			if ( !isset( $vulns_deduped[$vuln['id']]['paths'] ) &&
				count( $vuln['from'] ) > 1 ) {
				$npm_pkg_arr = preg_split( "/@/", $vuln['from'][1] );
				$npm_pkg = implode( '', array_slice( $npm_pkg_arr, 0, -1 ) );
				if ( strpos( $vuln['from'][1], "@" ) === 0 ) { $npm_pkg = "@{$npm_pkg}";
				}
				$npm_pkg = npm_pkg_url( $npm_pkg );
				/* @phan-suppress-next-line PhanTypeMismatchDimAssignment */
				$vulns_deduped[$vuln['id']]['paths'] = "{$npm_pkg}... ";
			}
			if ( !isset( $vulns_deduped[$vuln['id']]['title'] ) ) {
				$vulns_deduped[$vuln['id']]['title'] = $vuln['title'];
			}
			if ( !isset( $vulns_deduped[$vuln['id']]['moduleName'] ) ) {
				$vulns_deduped[$vuln['id']]['moduleName'] = npm_pkg_url( $vuln['moduleName'] );
				if ( isset( $vuln['semver']['vulnerable'] ) &&
					count( $vuln['semver']['vulnerable'] ) > 0 ) {
					$vulns_deduped[$vuln['id']]['moduleName'] .= " " . implode( ", ", $vuln['semver']['vulnerable'] );
				}
			}
			if ( !isset( $vulns_deduped[$vuln['id']]['cve'] ) &&
				count( $vuln['identifiers']['CVE'] ) > 0 ) {
				$vulns_deduped[$vuln['id']]['cve'] = "(" .
					cve_url( implode( ", ", $vuln['identifiers']['CVE'] ) ) . ")";
			} else {
				$vulns_deduped[$vuln['id']]['cve'] = '';
			}
			if ( !isset( $vulns_deduped[$vuln['id']]['severity'] ) ) {
				$vulns_deduped[$vuln['id']]['severity'] = risk_icon( $vuln['severity'] ) .
					" **" . $vuln['severity'] . "** ";
			}
		}
		foreach ( $vulns_deduped as $k => $v ) {
			echo "| " . $v['title'] . " " . $v['cve'];
			echo "| " . $v['moduleName'] . " ";
			$paths = ( $v['path_count'] > 1 ) ? "paths" : "path";
			echo "| " . $v['paths'] . " (plus " . $v['path_count'] . " $paths) ";
			echo "| [[ https://snyk.io/vuln | snyk ]] ";
			echo "| " . $v['severity'] . " ";
			echo "\n";
		}
	} elseif ( $mode == 'npm' ) {
		foreach ( $json_data_arr['advisories'] as $id => $adv ) {
			echo "| {$adv['title']} ";
			if ( is_array( $adv['cves'] ) && count( $adv['cves'] ) > 0 ) {
				echo "(" . cve_url( implode( ", ", $adv['cves'] ) ) . ") ";
			}
			echo "| " . npm_pkg_url( $adv['module_name'] ) . " " . re_pipes( $adv['vulnerable_versions'] ) . " ";
			echo "| " . proc_npm_findings( $adv['findings'] );
			echo "| [[ https://www.npmjs.com/advisories | npm ]] ";
			echo "| " . risk_icon( $adv['severity'] ) . " **{$adv['severity']}** ";
			echo "\n";
		}
	} else {
		throw new Exception( "Invalid mode - must be 'npm' or 'snyk'." );
	}
}
