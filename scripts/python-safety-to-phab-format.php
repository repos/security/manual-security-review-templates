#!/usr/bin/env php
<?php

$safety_json_output =
[

"{python safety json output array goes here}"

];

// phab output
echo <<<EOS2
| Package| Affected Version(s) | Installed Version | Vulnerability
| ---- | ---- | ---- | ---- 

EOS2;

$count = 1;
$vuln = [];
foreach ( $safety_json_output as $data ) {
	echo "| " . $data[0] . " | " . $data[1] . " | " . $data[2] . " | " . $data[3];
	echo "\n";
}
