#!/usr/bin/env php
<?php

$snyk_out_str = <<<EOS1

{snyk test cli text output goes here}

EOS1;

// helper functions
function npm_pkg_url( $pkg, $pkg_ver ) {
	return "[[ https://www.npmjs.com/package/{$pkg} | {$pkg_ver} ]]";
}

function npm_path( $package ) {
	$arr = preg_split( "/@/", $package );

	if ( count( $arr ) == 2 ) {
		$path = npm_pkg_url( $arr[0] ) . "@" . $arr[1];

	} else { $path = $package;
	}

	return $path;
}

function snyk_adv_url( $link_text, $url ) {
	return "[[ {$url} | {$link_text} ]]";
}

function service() {
	return "[[ https://security.snyk.io/ | snyk ]]";
}

function remed() {
	return "[see advisory link]";
}

function trim_long( string $str, string $adv_url, $len = 32 ) {
	$str = preg_replace( "/✗\s/", "", $str );

	if ( strlen( $str ) > $len ) {
		$str = substr( $str, 0, $len ) . "...";
	}

	$str = snyk_adv_url( $str, $adv_url );

	return $str;
}

function get_risk( $risk ) {
	$risks = [
		'Low' => "{icon check-circle color=green} **low**",
		'Medium' => "{icon exclamation-triangle color=yellow} **medium**",
		'High' => "{icon exclamation-triangle color=orange} **high**",
		'Critical' => "{icon exclamation-triangle color=red} **critical**"
	];

	return $risks[$risk];
}

// phab output
echo <<<EOS2
| Vulnerability | Package | Service | Remediation | Risk
| ---- | ---- | ---- | ---- | ----

EOS2;

// helper vars
$return_string = "More info";

$count = 0;
$vulns = [];
$str_arr = explode( "\n", $snyk_out_str );
foreach ( $str_arr as $line ) {
	$line_arr = [];
	$line = trim( $line );

	if ( preg_match( "/^✗/", $line ) ) {
		$line_proc = preg_split( "/\[(Low|Medium|High|Critical)\ Severity\]/",
			$line,
			-1,
			PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
		);

		preg_match( "/\[(https?:\/\/.+)\]/", $line_proc[2], $m );
		$adv_url = $m[1];
		preg_match( "/([^\s]+)@[^\s]+/", $line_proc[2], $m );
		$pkg = $m[1];
		$pkg_ver = $m[0];

		$vulns[$count]['vuln'] = trim_long( $line_proc[0], $adv_url );
		$vulns[$count]['pkg'] = npm_pkg_url( $pkg, $pkg_ver );
		$vulns[$count]['serv'] = service();
		$vulns[$count]['remed'] = remed();
		$vulns[$count]['risk'] = get_risk( $line_proc[1] );
	}

	$count++;
}

foreach ( $vulns as $v ) {
	echo "| " . $v['vuln'];
	echo " | " . $v['pkg'];
	echo " | " . $v['serv'];
	echo " | " . $v['remed'];
	echo " | " . $v['risk'];
	echo "\n";
}
