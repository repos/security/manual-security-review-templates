#!/usr/bin/env php
<?php

$scan_sh_out_str = <<<EOS1

{scan cli output table goes here}

EOS1;

/* helper functions */
function pkg_url( string $package ) {
	if ( strstr( $package, "@" ) ) {
		$package = substr( $package, 0, strpos( $package, "@" ) );
	}
	return "[[ https://www.npmjs.com/package/{$package} | {$package} ]]";
}

function advisory_link( string $url ) {
	return "[[ {$url} | advisory link ]]";
}

function trim_long( string $str, $len = 32 ) {
	if ( strlen( $str ) > $len ) {
		$str = substr( $str, 0, $len ) . "...";
	}

	// find some vuln texts
	if ( preg_match( "/((CVE\-\d+\-\d+))/", $str, $m ) ) {
		$str = str_replace( $m[1], "[[ https://nvd.nist.gov/vuln/detail/{$m[2]} | {$m[2]} ]]", $str );
	}

	return $str;
}

function service() {
	return "[[ https://slscan.io/ | scan ]]";
}

function get_risk( $risk ) {
	$risks = [
		'LOW' => "{icon check-circle color=green} **low**",
		'MEDIUM' => "{icon exclamation-triangle color=yellow} **medium**",
		'HIGH' => "{icon exclamation-triangle color=orange} **high**",
		'CRITICAL' => "{icon exclamation-triangle color=red} **critical**"
	];

	return $risks[$risk];
}

// phab output
echo <<<EOS2
| Vulnerability | Package | Notes | Service | Remediation | Risk
| ---- | ---- | ---- | ---- | ---- | ---- 

EOS2;

// helper vars
$vulns = [];

$processed_array = explode( "\n", $scan_sh_out_str );

foreach ( $processed_array as $line ) {
	if ( preg_match( "/^║\ (CVE\-\d+\-\d+)/", $line, $m ) ) {
		$data = preg_split( "/\s{1}\│\s{1}/", $line );
		$data = preg_replace( '/[^a-zA-Z0-9\s\-=+\|!@#$%^&*()`~\[\]{};:\'",<.>\/?]/', "", $data );
		$data = array_map( "trim", $data );
		if ( count( $m ) > 0 ) {
			$vulns[ $m[1] ]['vuln'] = trim_long( $m[1] );
			$vulns[ $m[1] ]['package'] = pkg_url( $data[1] );
			$vulns[ $m[1] ]['notes'] = "current: {$data[3]}; fixed in: {$data[4]}";
			$vulns[ $m[1] ]['risk'] = get_risk( $data[5] );
		}
	}
}

// display in phab remarkup
foreach ( $vulns as $vulndata ) {
	echo "| " . $vulndata['vuln'];
	echo " | " . $vulndata['package'];
	echo " | " . $vulndata['notes'];
	echo " | " . service();
	echo " | [see details within advisory links] ";
	echo " | " . $vulndata['risk'];
	echo "\n";
}
echo "\n";
