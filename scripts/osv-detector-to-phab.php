#!/usr/bin/env php
<?php

$osvd_out_str = <<<EOS1

{osv-scanner cli text output goes here}

EOS1;

// helper functions
function npm_pkg_url( $package, $ver = "" ) {
	return "[[ https://www.npmjs.com/package/{$package} | {$package}@{$ver} ]]";
}

function osvd_adv_url( $url ) {
	return "[[ {$url} | {$url} ]]";
}

function service() {
	return "[[ https://osv.dev/list | osv ]]";
}

function remediation() {
	return "[see advisory link]";
}

function risk() {
	return "[see advisory link]";
}

// phab output
echo <<<EOS2
| Vulnerability | Package | Service | Remediation | Risk
| ---- | ---- | ---- | ---- | ----

EOS2;

$count = 0;
$vuln = [];
$current_pkg = '';
$str_arr = explode( "\n", $osvd_out_str );
foreach ( $str_arr as $line ) {
	$line_proc = preg_replace( "/[\|\│\─\╭\╰\╮\╯]/", "", $line );
	$line_proc = trim( $line_proc );

	if ( preg_match( "/^https?:\/\/.*/", $line_proc ) ) {
		$vuln_data = preg_split( "/\s+/", $line_proc );
		echo "| " . osvd_adv_url( trim( $vuln_data[0] ) ) . " | ";
		$npm_key = null;
		$npm_key = array_search( "npm", $vuln_data );
		echo npm_pkg_url( $vuln_data[$npm_key + 1], $vuln_data[$npm_key + 3] ) . " | ";
		echo service() . " | ";
		echo remediation() . " | ";
		echo risk() . "\n";
	}
}
